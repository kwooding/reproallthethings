from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.0.1',
    description='''Reproducing EmbedAllTheThings by jc-healy and making it reproducible via easydata.''',
    author='John Healy, Amy Wooding',
    license='MIT',
)
